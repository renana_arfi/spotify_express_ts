import user_model from "./user.model.js";
import { User } from "../../utils/modules.types.js";

export class userRepository {
    async findAllUsers(){
        const users = await user_model.find();
        return users;
    }

    async findByIdUser(idToRead: string) {
        const user = await user_model.findById(idToRead);
        return user;
    }

    async findByIdAndRemovUser(idToDelete: string) {
        const user = await user_model.findByIdAndRemove(idToDelete);
        return user;
    }

    async createUser(user: User) {
        const newUser = await user_model.create(user);
        return newUser;
    }

    async findByIdAndUpdateUser(idToUpdate: string, user: User) {
        const newUser = await user_model.findByIdAndUpdate(idToUpdate, user, {
            new: true,
            upsert: false,
        });
        return newUser;
    }
    async getUserByEmail(userEmail:string) {
        const user = await user_model.findOne({'email': userEmail});
        return user;
      };

      async saveRefreshTokenOnUser(userEmail: string, refreshToken: string) {
        const user = await user_model.findOne({ email: userEmail });
        user.refresh_token = refreshToken;
        await user.save();
        return user;
    }
    async deleteRefreshToken(userEmail:string){
        const user = await user_model.findOne({ email: userEmail });
        user.refresh_token = null;
        await user.save();
        return user;
    }
    
}
