import user_model from "./user.model.js";
import { User } from "../../utils/modules.types.js";
import {userRepository} from "./user.sql.repo.js";
const userRepo = new userRepository();

export class userService {
    async readAllUsers() {
        const users = await userRepo.findAllUsers();
        return users;
    }

    async readUser(idToRead: string): Promise<User> {
        const user = await userRepo.findByIdUser(idToRead);
        return user;
    }

    async deleteUser(idToDelete: string) {
        const user = await userRepo.findByIdAndRemovUser(idToDelete);
        return user;
    }

    async createUser(user: User) {
        const newUser = await userRepo.createUser(user);
        return newUser;
    }

    async updateUser(idToUpdate: string, user: User) {
        const newUser = await userRepo.findByIdAndUpdateUser(idToUpdate, user);
        return newUser;
    }
    
}
