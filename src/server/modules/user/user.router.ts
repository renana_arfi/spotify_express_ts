/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import user_model from "./user.model.js";
import express,{Request,RequestHandler,Response} from 'express';
import {validateUser} from './user.validation.js'
import {userService} from './user.service.js'
import {HttpException} from '../../utils/error.types.js'
import {roles} from "../../utils/db.types.js"
import {verifyRoles} from "../../middleware/verify.js"
const US = new userService();
const router = express.Router();

// parse json req.body on post routes
router.use(express.json())

// creates new user
router.post("/", raw( async (req:Request, res:Response) => {
    const validUser = await validateUser(req.body,"createUser");
    const user = await US.createUser(validUser);
    res.status(200).json(user);
}) );

// get all users
router.get( "/",verifyRoles([roles.admin]),raw(async (req:Request, res:Response) => {
    const users = await US.readAllUsers();
    res.status(200).json(users);
  })  
);

// get a single user
router.get("/:id",raw(async (req:Request, res:Response) => {
    const user = await US.readUser(req.params.id);
    if (!user) throw new HttpException("user to read not found",404);
    res.status(200).json(user);
  })
);

// updates a single user
router.put("/:id",raw(async (req:Request, res:Response) => {
    const validUser = await validateUser(req.body,"updateUser");
    const user = await US.updateUser(req.params.id,validUser);
    if (!user) throw new HttpException("user to update not found",404);
    res.status(200).json(user);
  })
);


// DELETES A USER
router.delete("/:id",verifyRoles([roles.admin]),raw(async (req:Request, res:Response) => {
    const user = await US.deleteUser(req.params.id);
    if (!user) throw new HttpException("user to delete not found",404);
    res.status(200).json(user);
  })
);

//add pagination
router.get( "/paginate/:p/:c",raw(async (req:Request, res:Response) => {
  const users = await user_model.find()
                                // .select(`-__v`);
                                .select(`-_id 
                                        first_name 
                                        last_name 
                                        email 
                                        phone
                                        playlists`)
                                        .limit(Number(req.params.c))
                                        .skip(Number(req.params.p));
  res.status(200).json(users);
})  
);



export default router;
