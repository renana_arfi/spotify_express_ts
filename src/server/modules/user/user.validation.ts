import Joi from "joi";
import { HttpException } from "../../utils/error.types.js";
import { User } from "../../utils/modules.types.js";

export const validateUser = async (value: User, type: string) => {
    try {
        let name = Joi.string().alphanum().min(3).max(30);
        let email = Joi.string().email({
            minDomainSegments: 2,
            tlds: { allow: ["com", "net"] },
        });
        let password = Joi.string();
        let roles = Joi.string();

        if (type === "createUser") {
            name = name.required();
            email = email.required();
            password = password.required();
            roles = roles.required();
        }

        const schema = Joi.object().keys({
            name,
            email,
            password,
            roles
        });

        const user = await schema.validateAsync(value);
        return user;

    } catch (err:any) {
        throw new HttpException(err.details[0].message, 400);
    }
};
