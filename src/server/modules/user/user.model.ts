import mongoose from 'mongoose';
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    refresh_token: {type:String} ,
    playlists : [ { type: Schema.Types.ObjectId, ref:'playlist'} ],
    name   : { type:String,required:[true,'First name is required'] },
    email       : { type:String,required:true,unique:true},
    password : {type:String,required:true}
    },{timestamps:true});
  
export default model('user',UserSchema);