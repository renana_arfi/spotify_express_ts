import { User } from "../../utils/modules.types.js";
import {RowDataPacket} from "mysql2"
import { connection } from "../../db/nysql.connection.js";
import {HttpException} from "../../utils/error.types.js"
import bcrypt from 'bcryptjs';

export class userRepository {

    async findAllUsers(){
        const sql = `SELECT * FROM user`;
        const [rows] =await connection.query(sql);
        return rows;
    }

    async findByIdUser(idToRead: string) {
        const sql = `SELECT * FROM user WHERE user_id= ${idToRead}`;
        const results =await connection.query(sql);
        const result = results[0] as RowDataPacket
        if (result.length === 0) throw new HttpException(`User with id ${idToRead} was not found`,404);
        return result[0];
    }

    async findByIdAndRemovUser(idToDelete: string) {
        const user = await this.findByIdUser(idToDelete);
        const sql = `Delete from user where user_id = "${idToDelete}"` ;
        await connection.query(sql);
        return user;
    }

    async createUser(payload: User) {
        const hashPassword = await bcrypt.hash(payload.password, 8);
        payload.password = hashPassword;
        const sql = `INSERT INTO user SET ?;`;
        const rows = await connection.query(sql,payload);
        const result = rows[0] as RowDataPacket;
        const user = await this.findByIdUser(result.insertId);
        return user;
    }

    async findByIdAndUpdateUser(idToUpdate: string, payload: User) {
        let sql = `UPDATE user SET ? WHERE user_id = "${idToUpdate}"`;
        await connection.query(sql,payload);
        const user = await this.findByIdUser(idToUpdate);
        return user;   
     }
    
    async getUserByEmail(userEmail:string) {
        let sql = `select * from user where email= "${userEmail}"`
        const results = await connection.query(sql);
        const result : RowDataPacket = results[0] as RowDataPacket;
        // if (result.length === 0) throw new HttpException(`User with email ${userEmail} was not found`,404);
        return result[0];
      };

      async saveRefreshTokenOnUser(userEmail: string, refreshToken: string) {
        let sql = `UPDATE user 
            SET refresh_token = "${refreshToken}"
            WHERE email = "${userEmail}"`;
        await connection.query(sql);
        const user =  await this.getUserByEmail(userEmail);
        return user; 
        }

     async deleteRefreshToken(userEmail:string){
        let sql = `UPDATE user 
            SET refresh_token = "null"
            WHERE email = "${userEmail}"`;
        await connection.query(sql);
        const user =  await this.getUserByEmail(userEmail);
        return user; 
    }
    
}
