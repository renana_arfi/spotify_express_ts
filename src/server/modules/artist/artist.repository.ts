import artist_model from "./artist.model.js";
import { Artist } from "../../utils/modules.types.js";

export class artistRepository {

    async findAllArtist() {
        const artists = await artist_model.find();
        return artists;
    }

    async findByIdArtist(idToRead: string) {
        const artist = await artist_model.findById(idToRead);
        return artist;
    }

    async createArtist(payload: Artist) {
        const artist = await artist_model.create(payload);
        return artist;
    }

    async findByIdAndRemoveArtist(idToDelete: string) {
        const artist = await artist_model.findByIdAndRemove(idToDelete);
        return artist;
    }

    async findByIdAndUpdateArtist(idToUpdate: string, payload: Artist) {
        const artist = await artist_model.findByIdAndUpdate(idToUpdate,payload,{new: true,upsert: false,});
        return artist;
    }
}
