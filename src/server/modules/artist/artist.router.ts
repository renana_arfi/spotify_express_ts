import raw from "../../middleware/route.async.wrapper.js";
import { artistService } from "./artist.service.js";
import express, { Request, Response } from "express";
import { HttpException } from "../../utils/error.types.js";
import {roles} from "../../utils/db.types.js"
import {verifyRoles} from "../../middleware/verify.js"
// import song_model from "./song.model.js";
import {validateArtist} from './artist.validation.js'
// import {userService} from './user.logic.js'

const AS = new artistService();
const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// create artist
router.post(
    "/",verifyRoles([roles.user]),
    raw(async (req: Request, res: Response) => {
        const validArtist = await validateArtist(req.body,"createArtist");
        const artist = await AS.createArtist(validArtist);
        res.status(200).json(artist);
    })
);

// get all artists
router.get(
    "/",verifyRoles([roles.user]),
    raw(async (req: Request, res: Response) => {
        const artists = await AS.readAllArtist();
        res.status(200).json(artists);
    })
);

// get single artist
router.get(
    "/:id",verifyRoles([roles.user]),
    raw(async (req: Request, res: Response) => {
        const user = await AS.readArtist(req.params.id);
        if (!user) throw new HttpException("user to read not found", 404);
        res.status(200).json(user);
    })
);

// update details artist
router.put(
    "/:id",verifyRoles([roles.moderator]),
    raw(async (req: Request, res: Response) => {
        const validArtist = await validateArtist(req.body,"updateUser");
        const user = await AS.updateArtist(req.params.id, validArtist);
        if (!user) throw new HttpException("artist to update not found", 404);
        res.status(200).json(user);
    })
);

// delete  artist
router.delete(
    "/:id",verifyRoles([roles.moderator]),
    raw(async (req: Request, res: Response) => {
        const artist = await AS.deleteArtist(req.params.id);
        if (!artist) throw new HttpException("artist to delete not found", 404);
        res.status(200).json(artist);
    })
);

export default router;
