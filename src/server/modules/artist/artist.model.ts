import mongoose from 'mongoose';
import { Artist } from '../../utils/modules.types.js';
const { Schema, model } = mongoose;

export const ArtistSchema = new Schema<Artist>({
    first_name  : { type : String, required : true },
    last_name   : { type : String, required : true },
    songs       : [ { type: Schema.Types.ObjectId, ref:'song'} ],
}, {timestamps:true});
  
export default model('artist',ArtistSchema);
