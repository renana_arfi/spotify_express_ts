import Joi from "joi";
import { HttpException } from "../../utils/error.types.js";
import { Artist } from "../../utils/modules.types.js";

export const validateArtist = async (value: Artist, type: string) => {
    try {
        let first_name = Joi.string().alphanum().min(3).max(30);
        let last_name = Joi.string().alphanum().min(3).max(30);
        let status = Joi.string();
        if (type === "createArtist") {
            first_name = first_name.required();
            last_name = last_name.required();            
        }

        const schema = Joi.object().keys({
            first_name,
            last_name,
            status
        });

        const artist = await schema.validateAsync(value);
        return artist;

    } catch (err:any) {
        throw new HttpException(err.details[0].message, 400);
    }
};
