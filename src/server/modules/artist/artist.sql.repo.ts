import { connection } from "../../db/nysql.connection.js";
import { Status } from "../../utils/db.types.js";
import { Artist } from "../../utils/modules.types.js";
import {RowDataPacket} from "mysql2"
import {HttpException} from "../../utils/error.types.js"

export class artistRepository {
    async findAllArtist() {
        const sql = `SELECT * FROM artist WHERE status = "${Status.approve}"`;
        const [rows] =await connection.query(sql);
        return rows;
    }

    async findByIdArtist(artist_id: string,stat?:string) {
        const sql = `SELECT * 
         FROM artist
         WHERE artist_id = ${artist_id} AND status = "${stat || Status.approve}"`;
        const results =await connection.query(sql);
        const result = results[0] as RowDataPacket;
        if (result.length === 0) throw new HttpException(`artist with id ${artist_id} was not found`,404);
        return result[0];
    }

    async createArtist(payload: Artist) {
        console.log(payload)
        const { first_name, last_name } = payload;
        const sql = `INSERT INTO artist (first_name, last_name, status)
        VALUES ( "${first_name}", "${last_name}", "${Status.pending}");`;
        const rows = await connection.query(sql);
        const result  = rows[0] as RowDataPacket;
        const artist = await this.findByIdArtist(result.insertId,"pending");
        return artist; 
    }

    async findByIdAndUpdateArtist(idToUpdate: string, payload: Artist) {
        let sql = `UPDATE artist 
            SET ?
            WHERE artist_id = ?`;
        await connection.query(sql,[payload,idToUpdate]);
        const artist = await this.findByIdArtist(idToUpdate);
        return artist; 
    }

    async findByIdAndRemoveArtist(isToRemove: string){
        const artist = await this.findByIdArtist(isToRemove);
        const sql = `Delete from artist where artist_id ="${isToRemove}";` ;
        await connection.query(sql);
        return artist; 
    }
}
