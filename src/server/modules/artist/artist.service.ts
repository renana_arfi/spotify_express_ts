import { Artist } from "../../utils/modules.types.js";
import { songService } from "../song/song.service.js";
// import { artistRepository } from "./artist.repository.js";
import { artistRepository } from "./artist.sql.repo.js"
const SS = new songService();
const artistRepo = new artistRepository();
export class artistService {

    async readAllArtist() {
        const artists = await artistRepo.findAllArtist();
        console.log(artists)
        return artists;
    }

    async readArtist(idToRead: string) {
        const artist = await artistRepo.findByIdArtist(idToRead)
        return artist;
    }

    async createArtist(payload: Artist) {
        const artist = await artistRepo.createArtist(payload);
        return artist;
    }

    async deleteArtist(idToDelete: string) {
        const artist = await artistRepo.findByIdAndRemoveArtist(idToDelete);
        // artist.songs.foreach((idSong: string) => SS.deleteSong(idSong))
        return artist;
    }

    async updateArtist(idToUpdate: string, payload: Artist) {
        
        const artist = await artistRepo.findByIdAndUpdateArtist(idToUpdate,payload);
        return artist;
    }
}
