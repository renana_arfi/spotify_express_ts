import { userRepository } from "../user/user.sql.repo.js";
import { HttpException } from "../../utils/error.types.js";
import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import { User } from "../../utils/modules.types.js";

const {
    CLIENT_ORIGIN,
    APP_SECRET,
    ACCESS_TOKEN_EXPIRATION,
    REFRESH_TOKEN_EXPIRATION,
} = process.env;

const userRepo = new userRepository();

export class authService {
    async signup(name: string, email: string, password: string,roles:string) {
        // check that the email dont used on users db
        const checkUser = await userRepo.getUserByEmail(email);
        if (checkUser) throw new HttpException("this email already used", 404);
        // create user
        const payload = {
            name: name,
            email: email,
            password: password,
            roles:roles
        };   
        await userRepo.createUser(payload);
        const access_token = this.genrate_access_token(email);
        const refresh_token = await this.genrate_refresh_token(email);
        return {"access_token":access_token,"refresh_token":refresh_token};   
    }

    async login(email: string, password: string) {
        // check that the email  used on users db
        const user = await userRepo.getUserByEmail(email);
        if (!user) {
            throw new HttpException("user with that email don't exist", 404);
        }
        // check that the password right 
        const compare_password = await bcrypt.compare(password, user.password);
        if (!compare_password) {
            throw new HttpException("wrong email or password", 404);
        }
        const access_token = this.genrate_access_token(email);
        const refresh_token = await this.genrate_refresh_token(email);
        return {"access_token":access_token,"refresh_token":refresh_token};
    }

    genrate_access_token(userEmail: string) {
        const access_token = jwt.sign(
            { email: userEmail },
            APP_SECRET as string,
            {
                expiresIn: ACCESS_TOKEN_EXPIRATION, // expires in 10 minute
            }
        );
        return access_token;
    }

    async genrate_refresh_token(userEmail: string){
        const refresh_token = jwt.sign(
            { email: userEmail },
            APP_SECRET as string,
            {
                expiresIn: REFRESH_TOKEN_EXPIRATION, // expires in 60 days... long-term...
            }
        );
        await this.saveRefreshOnDb(userEmail, refresh_token);
        return refresh_token;
    }

    async saveRefreshOnDb(userEmail: string, refresh_token: string) {
        await userRepo.saveRefreshTokenOnUser(
            userEmail,
            refresh_token
        );
    }

    async logout(userEmail: string) {
        await userRepo.deleteRefreshToken(userEmail);
    }

    async verifyRefreshTokenDB(userEmail: string, refresh_token: string) {
        const user = await userRepo.getUserByEmail(userEmail);
        if(!user){
            throw new HttpException("user with that email don't exist", 404);
        }  
        if(user.refresh_token !== refresh_token){
            throw new HttpException("Unauthorized - Failed to verify refresh_token ", 404);
        }   
    }
}
