import raw from "../../middleware/route.async.wrapper.js";
import express, { Request, Response,RequestHandler } from "express";
import jwt from "jsonwebtoken";
import ms from "ms";
import cookieParser from "cookie-parser";
import { authService } from "./auth.service.js";
import { HttpException } from "../../utils/error.types.js";
const { CLIENT_ORIGIN , ACCESS_TOKEN_EXPIRATION,REFRESH_TOKEN_EXPIRATION,APP_SECRET } = process.env;
const APP_SECRET_STRING = APP_SECRET as string;
const AS = new authService();

const router = express.Router();
router.use(cookieParser());
router.use(express.json());

router.post(
    "/login",
    raw(async (req: Request, res: Response) => {
        const { email, password } = req.body;
        const {access_token,refresh_token}= await AS.login(email, password);
        res.cookie("refresh_token", refresh_token, {
            maxAge: ms(REFRESH_TOKEN_EXPIRATION as string), //60 days
            httpOnly: true,
        });
        // res.cookie("access_token", access_token, {
        //     maxAge: ms(ACCESS_TOKEN_EXPIRATION as string), //60 days
        //     httpOnly: true,
        // });
        res.status(200).json({
            URL: CLIENT_ORIGIN,
            email: email,
            access_token:access_token
        });
    })
);

router.post(
    "/signup",
    raw(async (req: Request, res: Response) => {
        const { name, email, password,roles } = req.body;
        console.log("before signup")
        const {access_token,refresh_token}= await AS.signup(name, email, password,roles);
        res.cookie("refresh_token", refresh_token, {
            maxAge: ms(REFRESH_TOKEN_EXPIRATION as string), //60 days
            httpOnly: true,
        });
        res.status(200).json({
            URL: CLIENT_ORIGIN,
            email: email,
            access_token:access_token
        });
    })
);

router.get(
    "/logout",
    raw(async (req: Request, res: Response) => {
        await AS.logout(req.body.email);
        res.clearCookie("refresh_token");
        res.status(200).json({ status: "You are logged out" });
    })
);

router.get(
    "/get-access-token",
    raw(async (req: Request, res: Response) => {
        const { refresh_token } = req.cookies;
        console.log({ refresh_token });
        if (!refresh_token)  throw new HttpException("Unauthorized-No refresh_token provided",403)
        // verifies secret and checks if valid 
        const decoded = await jwt.verify(refresh_token, APP_SECRET as string) as jwt.JwtPayload;
        console.log({ decoded });
        const { email } = decoded;
        //check user refresh token in DB
        await AS.verifyRefreshTokenDB(email,refresh_token);
        const access_token = AS.genrate_access_token(email);
        res.status(200).json({
            URL: CLIENT_ORIGIN,
            email: email,
            access_token:access_token
        });

    })
);

export default router;
