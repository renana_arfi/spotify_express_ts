import mongoose from 'mongoose'
import { Playlist } from '../../utils/modules.types.js';
const { Schema, model } = mongoose
import {SongSchema} from '../song/song.model.js'

export const PlaylistSchema = new Schema<Playlist>({
    title  : { type : String, required : true },
    description   : { type : String, required : true },
    length   : { type : Number, required : true },
    songs : [ SongSchema ],
}, {timestamps:true});
  
export default model('playlist',PlaylistSchema);