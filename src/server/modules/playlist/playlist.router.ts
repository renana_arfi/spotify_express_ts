import raw from "../../middleware/route.async.wrapper.js";
import {playlistService} from "./playlist.service.js"
import express,{Request,RequestHandler,Response} from 'express';
import {HttpException} from '../../utils/error.types.js'
import {validatePlaylist} from './playlist.validation.js'
import {roles} from "../../utils/db.types.js"
import {verifyRoles} from "../../middleware/verify.js"

// import song_model from "./song.model.js";
// import {userService} from './user.logic.js'

const PS = new playlistService();
const router = express.Router();
// parse json req.body on post routes
router.use(express.json())

// createplaylist
router.post("/",verifyRoles([roles.user]) ,raw( async (req:Request, res:Response) => {
    req.body.length = 0;
    const {email} = req.body;
    const validPlaylist = await validatePlaylist(req.body,"createPlaylist");
    const playlist = await PS.createPlaylist(validPlaylist,email);
    res.status(200).json(playlist);
}) );

// getAllplaylistsofuser
router.get( "/",verifyRoles([roles.user]),raw(async (req:Request, res:Response) => {
    const {email} = req.body;
    const playlists = await PS.getAllPlaylists(email);
    res.status(200).json(playlists);
  })  
);

// getSinglePlaylist
router.get("/:id",verifyRoles([roles.user]), raw(async (req:Request, res:Response) => {
  const {email} = req.body;
  const playlist = await PS.getSinglePlaylist(req.params.id,email);
    if (!playlist) throw new HttpException("playlist to read not found",404);
    res.status(200).json(playlist);
  })
);

// update playlist
router.put("/:id",verifyRoles([roles.user]), raw(async (req:Request, res:Response) => {
  const {email} = req.body;
    const validPlaylist = await validatePlaylist(req.body,"updatePlaylist");
    const playlist = await PS.updateplaylist(validPlaylist, req.params.id,email);
    if (!playlist) throw new HttpException("playlist to update not found",404);
    res.status(200).json(playlist);
  })
);


// delete playlist
router.delete("/:id",verifyRoles([roles.user]), raw(async (req:Request, res:Response) => {
    const {email} = req.body;
    const playlist = await PS.deleteplaylist(req.params.id,email);
    if (!playlist) throw new HttpException("playlist to delete not found",404);
    res.status(200).json(playlist);
  })
);

// add song to playlist
router.put("/:idPlaylist/song/:idSong",verifyRoles([roles.user]),  raw(async (req:Request, res:Response) => {
    const {email} = req.body;
    const { idPlaylist, idSong } = req.params;
    const playlist = await PS.addSongToPlaylist(idPlaylist, idSong,email);
    res.status(200).json(playlist);
}))  

// delete song from playlist
router.delete("/:idPlaylist/song/:idSong",verifyRoles([roles.user]),  raw(async (req:Request, res:Response) => {
  const {email} = req.body;
  const { idPlaylist, idSong } = req.params;
  const playlist = await PS.deleteSongFromPlaylist(idPlaylist, idSong,email);
  res.status(200).json(playlist);
}))  
export default router;
