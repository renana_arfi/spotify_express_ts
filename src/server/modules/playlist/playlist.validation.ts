import Joi from "joi";
import { HttpException } from "../../utils/error.types.js";
import { Playlist } from "../../utils/modules.types.js";

export const validatePlaylist = async (value: Playlist, type: string) => {
    try {
        let title = Joi.string().min(3).max(20);
        let description = Joi.string().min(3).max(50);
        let length;

        if (type === "createPlaylist") {
            title = title.required();
            description = description.required();
            length = Joi.number().equal(0);

        }

        const schema = Joi.object().keys({
            title,
            description,
            length,
        });

        const playlist = await schema.validateAsync(value);
        return playlist;

    } catch (err:any) {
        throw new HttpException(err.details[0].message, 400);
    }
};
