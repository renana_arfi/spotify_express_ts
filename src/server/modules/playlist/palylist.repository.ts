import playlist_model from "./playlist.model.js";
import song_model from "../song/song.model.js";
import { Playlist, Song } from "../../utils/modules.types.js";

export class playlistRepository {

    async findAllPlaylists() {
        const playlists = await playlist_model.find();
        return playlists;
    }

    async findByIdPlaylist(idPlaylist: string) {
        const playlist = await playlist_model.findById(idPlaylist);
        return playlist;
    }

    async createPlaylist(payload: Playlist) {
        let playlist = await playlist_model.create(payload);
        return playlist;
    }

    async findByIdAndUpdatePlaylist(payload: Playlist, idPlaylist: string) {
        const playlist = await playlist_model.findByIdAndUpdate(
            idPlaylist,
            payload,
            { new: true, upsert: true }
        );
        return playlist;
    }

    async findByIdAndRemoveplaylist(idplaylist: string) {
        const playlist = await playlist_model.findByIdAndRemove(idplaylist);
        return playlist;
    }

    async deleteFromPlaylistOfSongPlaylist(songs:string[],idplaylist:string ){
        songs.forEach(async(idSong:string) => {
            const song = await song_model.findById(idSong);
            song.playlists.pull(idplaylist);
            await song.save();
        });
    }
    async addSongToPlaylist(idPlaylist:string, idSong:string) {
        const song = await this.addPlaylistToSong(idPlaylist,idSong)
        const playlist = await playlist_model.findById(idPlaylist);
        playlist.songs.push(song);
        playlist.length += song.length;
        await playlist.save();
        return playlist;
    };

    async addPlaylistToSong(idPlaylist:string, idSong:string){
        const song = await song_model.findById(idSong);
        song.playlists.push(idPlaylist);
        await song.save();
        return song;
    }
    async deleteSongFromPlaylist(idPlaylist:string, idSong:string) {
        const song = await this.deletePlaylistFromSong(idPlaylist,idSong);
        const playlist = await playlist_model.findById(idPlaylist);
        playlist.songs.pull(song);
        playlist.length -= song.length;
        await playlist.save();
        return playlist;
    };

    async deletePlaylistFromSong(idPlaylist:string, idSong:string){
        const song = await song_model.findById(idSong);
        song.playlists.pull(idPlaylist);
        await song.save();
        return song;
    }
}
