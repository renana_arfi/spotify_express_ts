import { Playlist } from "../../utils/modules.types.js";
// import {playlistRepository} from "../playlist/palylist.repository.js";
import {playlistRepository} from "../playlist/playlist.sql.repo.js";
import {userRepository} from "../user/user.sql.repo.js"

const userRepo = new userRepository();
const playlistRepo = new playlistRepository();

export class playlistService {
    async getAllPlaylists(email:string) {
        const user =await userRepo.getUserByEmail(email);
        const playlists = await playlistRepo.findAllPlaylists(user.user_id);
        return playlists;
    }

    async getSinglePlaylist(idPlaylist: string,email:string) {
        const user =await userRepo.getUserByEmail(email);
        const playlist = await playlistRepo.findByIdPlaylist(idPlaylist,user.user_id);
        return playlist;
    }

    async createPlaylist(payload: Playlist,email:string) {
        const user =await userRepo.getUserByEmail(email);
        let playlist = await playlistRepo.createPlaylist(payload,user.user_id);
        return playlist;
    }

    async updateplaylist(payload: Playlist, idPlaylist: string,email:string) {
        const user =await userRepo.getUserByEmail(email);
        const playlist = await playlistRepo.findByIdAndUpdatePlaylist(payload,idPlaylist,user.user_id);
        return playlist;
    }

    async deleteplaylist(idplaylist: string,email:string) {
        const user =await userRepo.getUserByEmail(email);
        const playlist = await playlistRepo.findByIdAndRemoveplaylist(idplaylist,user.user_id);
        // await playlistRepo.deleteFromPlaylistOfSongPlaylist(playlist.songs,idplaylist);
        return playlist;
    }

    async addSongToPlaylist(idPlaylist:string, idSong:string,email:string) {
        const user =await userRepo.getUserByEmail(email);
        const playlist = await playlistRepo.addSongToPlaylist(idPlaylist,idSong,user.user_id);
        return playlist;
    };

    async deleteSongFromPlaylist(idPlaylist:string, idSong:string,email:string) {
        const user =await userRepo.getUserByEmail(email);
        const playlist = await playlistRepo.deleteSongFromPlaylist(idPlaylist,idSong,user.user_id);
        return playlist;
    };
}
