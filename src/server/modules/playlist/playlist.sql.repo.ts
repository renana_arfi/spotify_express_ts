import { connection } from "../../db/nysql.connection.js";
import { Playlist } from "../../utils/modules.types.js";
import {RowDataPacket} from "mysql2"
import {HttpException} from "../../utils/error.types.js"

export class playlistRepository {

    async findAllPlaylists(user_id:string) {
        const sql = `SELECT p.playlist_id,p.title,p.description,p.length, s.name as song_name
        FROM playlist as p
        LEFT JOIN playlist_song as ps
            ON p.playlist_id = ps.playlist_id
        LEFT JOIN song as s
            ON ps.song_id = s.song_id
         WHERE user_id= ${user_id}`;
        const [rows] =await connection.query(sql);
        return rows;
    }

    async findByIdPlaylist(idPlaylist: string, user_id:string) {
        const sql = `SELECT p.playlist_id,p.title,p.description,p.length, s.name as song_name
        FROM playlist as p
        LEFT JOIN playlist_song as ps
            ON p.playlist_id = ps.playlist_id
        LEFT JOIN song as s
            ON ps.song_id = s.song_id
        WHERE playlist_id = ${idPlaylist} AND user_id = ${user_id} `;
        const results =await connection.query(sql);
        const result = results[0] as RowDataPacket;
        if (result.length === 0) throw new HttpException(`playlist with id ${idPlaylist} was not found`,404);
        return result[0];
    }

    async createPlaylist(payload: Playlist,user_id:string) {
        const {title ,description, length} = payload;
        const sql = `INSERT INTO playlist ( title ,description, length,user_id)
        VALUES ("${title}","${description}", ${length}, "${user_id}");`;
        const rows = await connection.query(sql);
        const result  = rows[0] as RowDataPacket;
        const playlist = await this.findByIdPlaylist(result.insertId,user_id);
        return playlist; 
    }

    async findByIdAndUpdatePlaylist(payload: Playlist, idPlaylist: string, user_id:string) {
        const {title ,description, length} = payload;
        let sql = `UPDATE artist 
        SET title = "${title}",description= "${description}",length= ${length}
        WHERE  playlist_id = ${idPlaylist} AND user_id = ${user_id}`;
        const rows = await connection.query(sql);
        const result  = rows[0] as RowDataPacket;
        const playlist = await this.findByIdPlaylist(result.insertId,user_id);
        return playlist; 
    }

    async findByIdAndRemoveplaylist(idplaylist: string, user_id:string) {
        const playlist = await this.findByIdPlaylist(idplaylist,user_id);
        const sql = `Delete from playlist where playlist_id = ${idplaylist} AND user_id = ${user_id}` ;
        await connection.query(sql);
        return playlist; 
    }

    async addSongToPlaylist(idPlaylist:string, idSong:string,user_id:string) {
        const sql = `INSERT INTO playlist_song ( playlist_id, song_id)
        SELECT (${idPlaylist},${idSong}) 
        FROM playlist
        WHERE user_id=${user_id};`;
        const rows = await connection.query(sql);
        const result  = rows[0] as RowDataPacket;
        const playlist = await this.findByIdPlaylist(result.insertId,user_id);
        return playlist; 
    };

    async deleteSongFromPlaylist(idPlaylist:string, idSong:string,user_id:string) {
        const sql = `Delete ps from playlist_song as ps 
        join playlist as p on p.playlist_id = ps.playlist_id
        where ps.playlist_id = ${idPlaylist} AND ps.song_id =${idSong} AND p.user_id = ${user_id}` ;
        const rows = await connection.query(sql);
        const result  = rows[0] as RowDataPacket;
        const playlist = await this.findByIdPlaylist(result.insertId,user_id);
        return playlist; 
    };

}
