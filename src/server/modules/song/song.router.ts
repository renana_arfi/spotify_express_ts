import raw from "../../middleware/route.async.wrapper.js";
import {songService} from "./song.service.js"
import express,{Request,RequestHandler,Response} from 'express';
import {HttpException} from '../../utils/error.types.js'
import {validateSong} from './song.validation.js';
import {roles} from "../../utils/db.types.js"
import {verifyRoles} from "../../middleware/verify.js"
const SS = new songService();
const router = express.Router();
// parse json req.body on post routes
router.use(express.json())

// createSong 
router.post("/artist/:idArtist",verifyRoles([roles.user]), raw( async (req:Request, res:Response) => {
    const validSong = await validateSong(req.body,"createSong");
    const song = await SS.createSong(validSong,req.params.idArtist);
    res.status(200).json(song);
}) );

// getAllSongs
router.get( "/",verifyRoles([roles.user]),raw(async (req:Request, res:Response) => {
    const songs = await SS.getAllSongs();
    res.status(200).json(songs);
  })  
);

// getSingleSong
router.get("/:id",verifyRoles([roles.user]),raw(async (req:Request, res:Response) => {
    const song = await SS.getSingleSong(req.params.id);
    if (!song) throw new HttpException("song to read not found",404);
    res.status(200).json(song);
  })
);

// update song
router.put("/:id",verifyRoles([roles.moderator]),raw(async (req:Request, res:Response) => {
    const validSong = await validateSong(req.body,"updateUser");
    const song = await SS.updateSong(validSong, req.params.id);
    if (!song) throw new HttpException("song to update not found",404);
    res.status(200).json(song);
  })
);


// delete song
router.delete("/:id",verifyRoles([roles.moderator]),raw(async (req:Request, res:Response) => {
    const song = await SS.deleteSong(req.params.id);
    if (!song) throw new HttpException("song to delete not found",404);
    res.status(200).json(song);
  })
);


export default router;
