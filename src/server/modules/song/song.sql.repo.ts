import { Status } from "../../utils/db.types.js";
import { Song } from "../../utils/modules.types.js";
import { connection } from "../../db/nysql.connection.js";
import {RowDataPacket} from "mysql2"
import {HttpException} from "../../utils/error.types.js"

export class songRepository {

    async findAllSongs() {
        const sql = `SELECT s.song_id,s.name,s.genre,s.length, artist.first_name,artist.last_name
        FROM song as s
            JOIN artist on s.artist_id= artist.artist_id
        WHERE s.status= "${Status.approve}"`;
        const [rows] =await connection.query(sql);
        return rows;
    }

    async findByIdSong(idSong: string,stat?:string) {
        const sql = `SELECT s.song_id,s.name,s.genre,s.length,Concat(a.first_name," ",a.last_name) AS artist_name
        FROM song as s
            JOIN artist as a on s.artist_id= a.artist_id
        WHERE s.status= "${stat||Status.approve}" AND s.song_id = ${idSong}`;
        const results =await connection.query(sql);
        const result = results[0] as RowDataPacket;
        if (result.length === 0) throw new HttpException(`song with id ${idSong} was not found`,404);
        return result[0];
    }

    async createSong(payload: Song,idArtist:string) {
        const {name ,genre, length} = payload;
        const sql = `INSERT INTO song (artist_id, name ,genre, length,status)
        VALUES ( ${idArtist}, "${name}","${genre}",${length}, "${Status.pending}");`;
        const rows = await connection.query(sql);
        const result  = rows[0] as RowDataPacket;
        const song = await this.findByIdSong(result.insertId,"pending");
        return song; 
    }

    async findByIdAndRemoveSong(idSong: string) {
        const song = await this.findByIdSong(idSong);
        const sql = `Delete from song where song_id = ${idSong}` ;
        await connection.query(sql);
        return song; 
    }

    async findByIdAndUpdateSong(payload: Song, idSong: string) {
        const {  name ,genre, length} = payload;
        let sql = `UPDATE artist 
            SET name ="${name}",genre= "${genre}",length= ${length}
            WHERE song_id = ${idSong}`;
        const rows = await connection.query(sql);
        const result  = rows[0] as RowDataPacket;
        const song = await this.findByIdSong(result.insertId);
        return song; 
    }
}
