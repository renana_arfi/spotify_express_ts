// import {songRepository} from "./song.repository.js";
import {songRepository} from "./song.sql.repo.js"
import { Song } from "../../utils/modules.types.js";

const songRepo = new songRepository();
export class songService {
    async getAllSongs() {
        const songs = await songRepo.findAllSongs();
        return songs;
    }

    async getSingleSong(idSong: string) {
        const song = await songRepo.findByIdSong(idSong);
        return song;
    }

    async createSong(payload: Song, idArtist: string) {
        const song = await songRepo.createSong(payload,idArtist);
        // await songRepo.addSongToArtist(idArtist,song.id);
        return song;
    }

    async deleteSong(idSong: string) {
        const song = await songRepo.findByIdAndRemoveSong(idSong);
        // await songRepo.deleteSongFromArtist(idSong ,song.artist);
        // await songRepo.deleteSongFromHisPlaylist(idSong,song.playlists)
        return song;
    }

    async updateSong(payload: Song, idSong: string) {
        const song = await songRepo.findByIdAndUpdateSong( payload,idSong);
        // await songRepo.updatePlaylistsWithUpdateSong(song,song.playlists);
        return song;
    }

  
}
