import Joi from "joi";
import { HttpException } from "../../utils/error.types.js";
import { Song } from "../../utils/modules.types.js";

export const validateSong = async (value: Song, type: string) => {
    try {
        let name = Joi.string().min(3).max(30);
        let genre = Joi.string().alphanum().min(3).max(30);
        let length = Joi.number();
        let status = Joi.string();
        if (type === "createSong") {
            name = name.required();
            genre = genre.required();
            length = length.required();
        }

        const schema = Joi.object().keys({
            name,
            genre,
            length,
            status
        });

        const song = await schema.validateAsync(value);
        return song;

    } catch (err:any) {
        throw new HttpException(err.details[0].message, 400);
    }
};
