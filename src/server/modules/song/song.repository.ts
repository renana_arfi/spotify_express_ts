import song_model from "./song.model.js";
import artist_model from "../artist/artist.model.js";
import playlist_model from "../playlist/playlist.model.js";

import { Song } from "../../utils/modules.types.js";

export class songRepository {

    async findAllSongs() {
        const songs = await song_model.find();
        return songs;
    }

    async findByIdSong(idSong: string) {
        const song = await song_model.findById(idSong);
        return song;
    }

    async createSong(payload: Song) {
        const song = await song_model.create(payload);
        return song;
    }

    async addSongToArtist(idArtist:string,idSong:string){
        const artist = await artist_model.findById(idArtist);
        artist.songs.push(idSong);
        await artist.save();
        return artist;
    }


    async findByIdAndRemoveSong(idSong: string) {
        const song = await song_model.findByIdAndRemove(idSong);
        return song;
    }

    async deleteSongFromArtist(idSong: string, idArtist: string){
        const artist = await artist_model.findById(idArtist);
        if(artist){
            artist.songs.pull(idSong);
            await artist.save();
        }
    }

    async deleteSongFromHisPlaylist(idSong : string,playlists:string[]){
        playlists.forEach(async(idPlaylist:string)=>{
            const playlist = await playlist_model.findById(idPlaylist);
            playlist.songs.pull(idSong);
            await playlist.save();
        });
    }

    async findByIdAndUpdateSong(payload: Song, idSong: string) {
        const song = await song_model.findByIdAndUpdate(idSong, payload, {
            new: true,
            upsert: true,
        });
        return song;
    }

    async updatePlaylistsWithUpdateSong(song:Song, playlists : string[]){
        playlists.forEach(async(idPlaylist:string)=>{
            const playlist = await playlist_model.findById(idPlaylist);
            playlist.songs.update(song);
            await playlist.save();
        });
    }
}
