import mongoose from 'mongoose'
const { Schema, model } = mongoose
import {Song} from '../../utils/modules.types.js'

export const SongSchema = new Schema<Song>({
    name  : { type : String, required : true },
    genre   : { type : String, required : true },
    length : { type : Number, requird : true},
    artist :   { type: Schema.Types.ObjectId, ref:'artist'}  ,
    playlists : [ { type: Schema.Types.ObjectId, ref:'playlist'} ],
}, {timestamps:true});
  
export default model('song',SongSchema);