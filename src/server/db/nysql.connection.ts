import mysql from "mysql2/promise";
const {DB_HOST ,DB_PORT,DB_NAME,DB_USER_NAME,DB_USER_PASSWORD} = process.env;
import log from '@ajar/marker'

export let connection: mysql.Connection;

export const conect = async () => {
    if (connection) return connection;
    connection = await mysql.createConnection({
        host:DB_HOST ,
        port:Number(DB_PORT) ,
        user: DB_USER_NAME ,
        database:DB_NAME ,
        password: DB_USER_PASSWORD ,
    });
    await connection.connect();
    log.magenta(' ✨  Connected to MYSQL db ✨ ')
};