import fetch from "node-fetch";
import cron from "node-cron";
import fs from "fs";
import cors from "cors";
import log from "@ajar/marker";
import express, { Express } from "express";
import morgan from "morgan";
import { updateLogError, updateLogFile } from "./middleware/logger.handler.js";
import { connect_db } from "./db/mongoose.connection.js";
import artist_router from "./modules/artist/artist.router.js";
import song_router from "./modules/song/song.router.js";
import auth_router from "./modules/auth/auth.router.js";
import user_router from "./modules/user/user.router.js";
import playlist_router from "./modules/playlist/playlist.router.js";
import { addIdToRequest } from "./middleware/request.handler.js";
import {
    printError,
    responseError,
    not_found,
} from "./middleware/errors.handler.js";
import { verifyAuth } from "./middleware/verify.js";
import { conect } from "./db/nysql.connection.js";
import { exec } from "child_process";
import http, { ServerResponse } from "http";
const LOG_FILE_PATH = "./req.http.log";
const LOG_ERROR_PATH = "./req.error.log";
const PORT_backup = 3031;
const {
    PORT = 3030,
    HOST = "localhost",
    DB_URI = "mongodb://localhost:27017/crud-demo",
} = process.env;

class api {
    constructor() {
        const app = express();
        this.genericMiddleware(app);
        this.actionRouters(app);
        this.errorHandling(app);
        //start the express api server
        this.init(app);
        this.backup();
    }

    genericMiddleware(app: Express) {
        app.use(cors());
        app.use(morgan("dev"));
    }

    actionRouters(app: Express) {
        app.use(addIdToRequest);
        app.use(updateLogFile(LOG_FILE_PATH));
        // routing
        app.use("/api/artist", verifyAuth, artist_router);
        app.use("/api/song", verifyAuth, song_router);
        app.use("/api/playlist", verifyAuth, playlist_router);
        app.use("/api/user", verifyAuth, user_router);
        app.use("/api/auth", auth_router);
    }

    errorHandling(app: Express) {
        app.use(not_found);
        app.use(printError);
        app.use(updateLogError(LOG_ERROR_PATH));
        app.use(responseError);
    }

    async init(app: Express) {
        try {
            //connect to mongo db
            await conect();
            await app.listen(Number(PORT), HOST);
            log.magenta(
                "api is live on",
                ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`
            );
        } catch (err) {
            console.log(err);
        }
    }

    async backup() {
        const options = {
            host: HOST,
            path: "/backup",
            port: PORT_backup,
            method: "POST",
        };

        cron.schedule(" */1 * * * *", async () => {
            const req = http.request(options);
            console.log("in corn");
            const child = exec(
                "docker exec mysql-db /usr/bin/mysqldump -u root --password=qwerty music_db"
            );
            child.stdout?.pipe(req);
        });
    }
    //   cron.schedule("*/5 * * * * *",async () => {
    //             const child = exec("docker exec mysql-db /usr/bin/mysqldump -u root --password=qwerty music_db");
    //            await fetch(`http://${HOST}:${PORT_backup}/backup`,{method: "POST", body:child.stdout} )
    //            console.log("running a task every one minutes");
    //         });
    //     }
}

new api();
