import { NextFunction,Request,Response } from "express";
import jwt from 'jsonwebtoken'
import raw from "./route.async.wrapper.js"
import { userRepository } from "../modules/user/user.sql.repo.js";
import { HttpException } from "../utils/error.types.js";
const {CLIENT_ORIGIN, APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION} = process.env;
const userRepo = new userRepository();

export const verifyAuth = raw(async (req:Request, res:Response, next:NextFunction) => {
    const access_token = req.headers["access-token"] as string;

    if (!access_token) throw new HttpException("Unauthorized-No token provided",404)
    
    // verifies secret and checks exp
    const decoded = await jwt.verify(access_token , APP_SECRET as string) as jwt.JwtPayload
    // if everything is good, save to request for use in other routes
    req.email_user = decoded.email;
    next();
})

export const verifyRoles =  (roles:string[]) =>   raw(async (req:Request, res:Response, next:NextFunction) => {
    const user = await userRepo.getUserByEmail(req.email_user);
    const userRoles:string[]= user.roles.split(",");
    const valid = roles.some(role => userRoles.includes(role));
    if(valid) next();
    else throw new HttpException("Unverify-You do not have the appropriate permission",404);
})