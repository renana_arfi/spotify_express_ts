import { Request, Response, NextFunction } from "express";
import {HttpException} from "../utils/error.types.js"

import fsp from "fs/promises";
import fs from "fs"
//middelware - update log http-requests 

// export const updateLogFile = (path: string) => async(
//     req: Request,
//     res: Response,
//     next: NextFunction
//   ) => {
//     await fsp.writeFile(path, `req id - ${req.id} = ${req.method}: ${req.path} >> ${Date.now()}\n`, {
//       flag: "a",
//     });
//     next();
//   };

  export function updateLogFile(path){
    const ws = fs.createWriteStream(path);
    return(req: Request,res: Response,next: NextFunction)=>{
      ws.write(`req id - ${req.id} = ${req.method}: ${req.path} >> ${Date.now()}\n`)
      next();
    }
  }

//middelware - update Log-errors
export const updateLogError= (path:string) => async (err:HttpException,req:Request,res:Response,next:NextFunction)=>{
    await fsp.writeFile(path,`req id - ${req.id} = ${err.status} :: ${err.message} >> ${err.stack} \n`
    , {
        flag: "a",
      });
      next(err);
}