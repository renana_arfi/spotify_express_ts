export enum Status {
   approve = 'approve',
   pending = 'pending',
   reject = 'reject'
  }

export enum roles{
   admin = "1",
   moderator="2",
   user="3"
}