
import { cleanEnv, str, num } from "envalid";
export const environment = cleanEnv(process.env, {
    NODE_ENV: str({
        choices: ["development", "test", "production", "staging"],
    }),
    DB_HOST:str({ default: "localhost" }),
    DB_PORT: num({ default: 3030 }),
    DB_NAME:str({ default: "my_music_db" }),
    DB_USER_NAME:str({ default: "root" }),
    DB_USER_PASSWORD:str({ default: "qwertystr" })

});