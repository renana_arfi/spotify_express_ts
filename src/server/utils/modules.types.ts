import { Schema } from "mongoose";

export interface User {
    name: string;
    email: string;
    password:string;
    roles:string;
}
export interface Artist {
  first_name: string;
  last_name: string;
  songs: string[];
}

export interface Playlist {
    title: string;
    description: string;
    length : Number;
    songs: Song[];
}

export interface Song {
  name: string;
  genre: string;
  length:Number;
  artist:string;
  playlists: string[];
}

declare global {
    namespace Express {
      interface Request {
        id:string;
        email_user:string;
      }
    }
  }
  